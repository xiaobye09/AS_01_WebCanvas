# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
<img src="demo.jpg" width="700px" height="500px"></img>

* **完成全部的基礎功能:**   
    * Basic control tools:利用input標籤的type="color"實作顏色選擇(此時value會是#xxxxxx，顏色類型的)。利用input的type="number"來實作線粗細變化
    * Text input:用input的type="text"當接收字串，存起來準備作為印出用的。利用input的type="number"來實作字體大小，利用selector來選字體。
    * Cursor icon:利用js來改變css達到改變鼠標的目的
    * refresh button:直接畫一個白色畫布蓋上去，順便將redo/undo所使用的stack清空
* **完成全部的進階功能:**    
    * different brush shape:click事件觸發時，先記錄下當下的畫布，當move事件觸發時，先將剛剛紀錄的畫布蓋上去，每次的move都一定會用此塊畫布先蓋上復原，然後再開始作畫
    * redo/undo:使用一個stack來記錄每次變化的畫布
    * image tool:利用input的type="file"來讀取圖檔。若是沒有讀檔就畫，會跳出alert視窗
    * download:利用標籤a的download屬性來實作。
---
* **各個按鈕(滑鼠放上去皆有註解):**
    * 大刷子是繪圖筆，他的cursor icon是一個小小的圓點，使用小圓點是因為方便繪圖時瞄準。
    * 橡皮擦圖片就是橡皮擦，按下去就可以擦擦擦。
    * Aa圖案的則是文字，先在文字輸入框輸入後，點擊Aa移過去點一下就可以貼上來，可以連續點擊。字體顏色受最上面的選色扭影響，字型預設serif，字體大小預設20。
    * 一張圖片的按鈕可以在畫布上，蓋上自己上傳的圖片。
    * 箭頭向左的圖片是undo，向右是redo。
    * 空心三角形可以繪製空心的三角形，粗細受最上方的線條粗細輸入欄影響。
    * 實行矩形可以繪製實心的矩形，顏色受最上方的選色扭影響。
    * 空心圓型可以繪製空心的圓型﹐粗細受最上方的線條粗細輸入欄影響。
    * 雙箭頭的按鈕是reset紐，按下去會將畫布清空，且按下去也會清空redo/undo stack。
    * 畫布最右邊，有一個*下載畫布*，點擊後可以下載畫好的圖，預設名稱為untitled.jpg。
* **要求外的功能:**
    * 彩虹圖片是彩虹繪圖筆，createLinearGradient利用這個函數拿到一個有色階的strokeStyle，然後就可以以彩畫出彩虹。
    * 直線圖片是畫直線用的，可任意拖曳規劃直線方向。
