var drawing = false;
var brush_size = 20;
var brush_shape = "round"
var brush_color="#000000";
var cur_tool = "";
var img = new Image();
var canvasWidth;
var canvasHeight;
var ctx;
var canvas; 
//redo-undo stack
var img_stack = new Array();
var pos = -1;
//the variable that use in drawing polygon
var temp_img;
var start_x;
var start_y;

window.addEventListener("load", ()=>{
    console.log("hello");
    canvas = document.getElementById("mycanvas");
    canvasWidth = canvas.width = window.innerWidth*0.5;
    canvasHeight = canvas.height = window.innerHeight* 0.7;
    ctx = canvas.getContext("2d");
    //this two lines are important!!If remove them,our undo will fail since the default background color of canvas is transparent. 
    ctx.fillStyle = "white";
    ctx.fillRect(0,0,canvasWidth,canvasHeight);

    pushImg()//remember the first scene
    function drawImg(e)
    {
        ctx.drawImage(img,e.clientX-canvas.getBoundingClientRect().left,e.clientY-canvas.getBoundingClientRect().top);
        console.log("success to draw");
    }
    function StartDraw(e)
    {
        drawing = true;
        ctx.strokeStyle = brush_color;
        ctx.lineCap = brush_shape;
        ctx.lineWidth=brush_size;
        if(cur_tool==="0")
        {
            Draw(e);
        }
        else if(cur_tool==="1")
        {
            ctx.strokeStyle = "white";
            ctx.lineCap = "square"
            Draw(e);
        }
        else if(cur_tool==="2")
        {
            drawImg(e);
        }
        else if(cur_tool==="3")
        {
            temp_img = ctx.getImageData(0,0,canvasWidth,canvasHeight);
            start_x = e.clientX- canvas.getBoundingClientRect().left;
            start_y = e.clientY- canvas.getBoundingClientRect().top;
        }
        else if(cur_tool==="4")
        {
            temp_img = ctx.getImageData(0,0,canvasWidth,canvasHeight);
            start_x = e.clientX- canvas.getBoundingClientRect().left;
            start_y = e.clientY- canvas.getBoundingClientRect().top;
        }
        else if(cur_tool==="5")
        {
            temp_img = ctx.getImageData(0,0,canvasWidth,canvasHeight);
            start_x = e.clientX- canvas.getBoundingClientRect().left;
            start_y = e.clientY- canvas.getBoundingClientRect().top;
        }
        console.log("start drawing");
    }
    function FinishDraw()
    {
        drawing = false;
        ctx.beginPath();//reset the first drawing position
        
        if(cur_tool!="") {pushImg();}//remember the scence
        console.log("stop drawing");
    }
    function Draw(e)
    {
        if(!drawing) return;
        if(cur_tool==="0")
        {
            let bound = canvas.getBoundingClientRect();
            console.log("drawing");
            ctx.lineTo(e.clientX- canvas.getBoundingClientRect().left,e.clientY-canvas.getBoundingClientRect().top );
            ctx.stroke();
        }
        else if(cur_tool==="1")
        {
            console.log("erasing");
            ctx.lineTo(e.clientX-canvas.getBoundingClientRect().left ,e.clientY-canvas.getBoundingClientRect().top);
            ctx.stroke();
        }
        else if(cur_tool==="3")
        {
            let cur_x=e.clientX- canvas.getBoundingClientRect().left;
            let cur_y=e.clientY- canvas.getBoundingClientRect().top;
            
            ctx.putImageData(temp_img,0,0);
            
            ctx.beginPath();
        
            ctx.moveTo(start_x,start_y);
            ctx.lineTo(cur_x,cur_y);
            ctx.lineTo(cur_x-2*(cur_x-start_x),start_y+(cur_y-start_y));
            
            ctx.closePath();
            ctx.stroke();
        }
        else if(cur_tool==="4")
        {
            let cur_x=e.clientX- canvas.getBoundingClientRect().left;
            let cur_y=e.clientY- canvas.getBoundingClientRect().top;

            ctx.putImageData(temp_img,0,0);

            ctx.beginPath();
            
            ctx.moveTo(start_x,start_y);
            ctx.lineTo(cur_x,start_y);
            ctx.lineTo(cur_x,cur_y);
            ctx.lineTo(start_x,cur_y);
            
            ctx.closePath();
            ctx.stroke();
        }
        else if(cur_tool==="5")
        {
            ctx.putImageData(temp_img,0,0);

            ctx.beginPath();
            
            ctx.arc((e.clientX+start_x)/2,(e.clientY+start_y)/2,Math.pow(Math.pow(e.clientX-start_x,2)+Math.pow(e.clientY-start_y,2),0.5)/2,0,2*Math.PI);
            
            ctx.stroke();
        }
    }

    //event listener
    canvas.addEventListener("mouseup",FinishDraw);
    canvas.addEventListener("mousedown",StartDraw);
    canvas.addEventListener("mousemove",Draw);
});
function changeTool(type)
{
    var elementToChange = document.getElementsByTagName("body")[0];
    cur_tool = type;
    if(type==="0")//brush
    {
        console.log("chage cursor to brush");
        elementToChange.setAttribute("style","cursor:url('brush_cursor.png'), auto;");
    }
    else if(type==="1")//eraser
    {
        console.log("chage cursor to eraser");
        elementToChange.setAttribute("style","cursor:url('eraser_cursor.png'), auto;");
    }
    else if(type==="2")//img
    {
        console.log("chage cursor to draw img");
        elementToChange.setAttribute("style","cursor:url('draw_img_cursor.png'), auto;");
    }
    else if(type==="3")//triangle
    {
        console.log("chage cursor to draw triangle");
        elementToChange.setAttribute("style","cursor:url('draw_img_cursor.png'), auto;");
    }
    else if(type==="4")//rectangle
    {
        console.log("chage cursor to draw rectangle");
        elementToChange.setAttribute("style","cursor:url('draw_img_cursor.png'), auto;");
    }
    else if(type==="5")//triangle
    {
        console.log("chage cursor to draw circle");
        elementToChange.setAttribute("style","cursor:url('draw_img_cursor.png'), auto;");
    }
}
function changeBrushSize(size)
{
    brush_size = Number(size);
}
function changeBrushColor(color)
{
    brush_color = color;
    console.log("chage color to:"+color);
}
function changeBrushShape(shape)
{
    brush_shape=shape;
    console.log("change brush shape:"+shape);
}
function uploadImg(obj)
{
    console.log(obj.value);
    var reader = new FileReader();
	 // Read in the image file as a data URL.
	reader.readAsDataURL(obj.files[0]);
	reader.onload = function(evt){
        if( evt.target.readyState == FileReader.DONE) {
            img.src = evt.target.result;
            console.log("read file success");
        }
    }    
}
function reset()
{
    console.log("reset"+" (0,0) to"+" ("+canvasWidth+","+canvasHeight+")");
    ctx.fillStyle='white';
    ctx.fillRect(0,0,canvasWidth,canvasHeight);
    //clear the array
    img_stack.length=0;
    pos=-1;
    pushImg();
}
function pushImg()
{
    if(pos<img_stack.length-1){img_stack.length=pos+1;}
    img_stack.push(canvas.toDataURL());
    pos++;    
    console.log("push img " + "pos " +pos+" length "+img_stack.length);
}
function undo()
{
    if(pos>0)
    {
        pos--;
        var canvasImg = new Image();
        canvasImg.src = img_stack[pos];
        if(canvasImg.complete){ 
            ctx.drawImage(canvasImg, 0, 0);
            console.log("undo "+ "pos " +pos+" length "+img_stack.length); 
        }
        else{
            canvasImg.addEventListener("load", function() {
                ctx.drawImage(canvasImg, 0, 0);
                console.log("undo "+ "pos " +pos+" length "+img_stack.length); 
            }, false);
        }
        
        
    }
}
function redo()
{
    if(pos<img_stack.length-1)
    {
        pos++;
        var canvasImg = new Image();
        canvasImg.src = img_stack[pos];
        if(canvasImg.complete){ 
            ctx.drawImage(canvasImg, 0, 0);
            console.log("redo "+ "pos " +pos+" length "+img_stack.length); 
        }
        else{
            canvasImg.addEventListener("load", function() {
                ctx.drawImage(canvasImg, 0, 0);
                console.log("redo "+ "pos " +pos+" length "+img_stack.length); 
              }, false);
        }
    }
}
